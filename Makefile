PREFIX=/usr/local

all:
	c++ -O3 -march=native -pipe -o seconds-to-minutes seconds-to-minutes.cpp


install:
	mkdir -p ${PREFIX}/bin
	cp seconds-to-minutes ${PREFIX}/bin
	cp cmus-info ${PREFIX}/bin
	cp cmus-info-no-timestamp ${PREFIX}/bin
	cp cmus-info-notify ${PREFIX}/bin

uninstall:
	rm ${PREFIX}/bin/seconds-to-minutes
	rm ${PREFIX}/bin/cmus-info
	rm ${PREFIX}/bin/cmus-info-no-timestamp
	rm ${PREFIX}/bin/cmus-info-notify

deinstall:
	rm ${PREFIX}/bin/seconds-to-minutes
	rm ${PREFIX}/bin/cmus-info
	rm ${PREFIX}/bin/cmus-info-no-timestamp
	rm ${PREFIX}/bin/cmus-info-notify

clean:
	rm seconds-to-minutes
