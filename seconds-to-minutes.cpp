#include <iostream>

using std::cout; using std::cin; using std::endl; using std::string; using std::stoi; using std::to_string;


string stm(int total)
{
  string result;
  int seconds = (total % 60);
  total = ((total - seconds) / 60);

  if(seconds>=10)
    result = (to_string(total) + ":" + to_string(seconds));
  else
    result = (to_string(total) + ":0" + to_string(seconds));
  
  return result;
}

int main(int argc, char** argv)
{

  int s;
  int seconds;
  
  if(argc < 2)
    {
      cin >> s;
    }
  else
    s = stoi(argv[1]);

  cout << stm(s) << endl;
  
  return 0;
}
